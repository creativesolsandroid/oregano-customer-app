package com.cs.oregano.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.oregano.Constants;
import com.cs.oregano.JSONParser;
import com.cs.oregano.R;
import com.cs.oregano.widgets.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CS on 14-07-2016.
 */
public class ForgotPassword extends AppCompatActivity {

    Toolbar toolbar;
    private String response = null;
    TextView mCancel;
    Button mSend;
    EditText mEmail;
    SharedPreferences languagePrefs;
    String language;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.forgot_password);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.forgot_password_arabic);
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSend = (Button) findViewById(R.id.send_button);
        mEmail = (EditText) findViewById(R.id.forgot_email);

        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mEmail.getText().toString();
                if(email.length() == 0){
                    mEmail.setError("Please enter mobile number");
                }else{
                    new GetVerificationDetails().execute(Constants.FORGOT_PASSWORD_URL+"966"+email);
                }

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    public class GetVerificationDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(ForgotPassword.this);
            dialog = ProgressDialog.show(ForgotPassword.this, "",
                    "Sending OTP...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(ForgotPassword.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(ForgotPassword.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {

                            JSONObject jo = new JSONObject(result);
                            try {
                                JSONObject jo1 = jo.getJSONObject("Success");
                                String otp = jo1.getString("OTP");
                                String mobile = jo1.getString("MobileNo");
                                dialog.dismiss();
                                Intent loginIntent = new Intent(ForgotPassword.this, VerifyRandomNumber.class);
                                loginIntent.putExtra("phone_number", mobile);
                                loginIntent.putExtra("OTP", otp);
                                startActivity(loginIntent);
                            }catch (JSONException je){
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ForgotPassword.this);


//                            if(language.equalsIgnoreCase("En")) {
                                // set title
                                alertDialogBuilder.setTitle("Oregano");

                                // set dialog message
                                alertDialogBuilder
                                        .setMessage("Mobile number not registered")
                                        .setCancelable(false)
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                                onBackPressed();
                                            }
                                        });
//                            }else if(language.equalsIgnoreCase("Ar")){
//                                // set title
//                                alertDialogBuilder.setTitle("د. كيف");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("سوف يصلك منا قريباً بريد الكتروني موضح فيه كيف تفعل كلمة المرور")
//                                        .setCancelable(false)
//                                        .setPositiveButton("تم", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
//                            }

                                // create alert dialog
                                AlertDialog alertDialog = alertDialogBuilder.create();

                                // show it
                                alertDialog.show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(ForgotPassword.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

}
