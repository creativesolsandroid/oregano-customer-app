package com.cs.oregano.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.oregano.JSONParser;
import com.cs.oregano.R;
import com.cs.oregano.adapters.CheeseRecyclerAdapter;
import com.cs.oregano.adapters.CurstRecyclerAdapter;
import com.cs.oregano.adapters.ProteinsRecyclerAdapter;
import com.cs.oregano.adapters.VeggiesRecyclerAdapter;
import com.cs.oregano.model.AdditionalPrices;
import com.cs.oregano.model.Additionals;
import com.cs.oregano.model.ItemPrices;
import com.cs.oregano.model.Modifiers;
import com.cs.oregano.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class CustomizePizza extends AppCompatActivity {

    Toolbar toolbar;
    TextView size;
    Spinner spinner;
    String selectedtext = " ";
    Integer indexValue = 0;
    ImageView mlefthalfPizza, mrighthalfPizza, mfullPizza;
    String itemId, categoryId, itemName, itemNameAr, itemDesc, itemDescAr, itemImage, isDelivery;

    private ArrayList<ItemPrices> pricesList = new ArrayList<>();
    private ArrayList<Modifiers> modifiersList = new ArrayList<>();
    private ArrayList<Modifiers> crustList = new ArrayList<>();

    public static ArrayList<String> selectedaddtionals = new ArrayList<>();

    ArrayList<Additionals> curst = new ArrayList<>();

    CurstRecyclerAdapter curstlist;
    VeggiesRecyclerAdapter veggieslist;
    CheeseRecyclerAdapter cheeselist;
    ProteinsRecyclerAdapter proteinslist;
    RecyclerView crustview, veggiesList, cheeseList, protenisList;

    String[] items;
    String response;
    SharedPreferences languagePrefs;
    String language;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        setContentView(R.layout.custumize_pizza);

        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        itemId = getIntent().getStringExtra("item_id");
        categoryId = getIntent().getStringExtra("category_id");
        itemName = getIntent().getStringExtra("item_name");
        itemDesc = getIntent().getStringExtra("item_desc");
        itemImage = getIntent().getStringExtra("item_image");
        itemNameAr = getIntent().getStringExtra("item_nameAr");
        itemDescAr = getIntent().getStringExtra("item_descAr");
        isDelivery = getIntent().getStringExtra("delivery");

        size = (TextView) findViewById(R.id.size);

        mlefthalfPizza = (ImageView) findViewById(R.id.left_half_pizza);
        mrighthalfPizza = (ImageView) findViewById(R.id.right_half_pizza);
        mfullPizza = (ImageView) findViewById(R.id.full_pizza);

        spinner = (Spinner) findViewById(R.id.spinner);

        crustview = (RecyclerView) findViewById(R.id.crust_listview);
        veggiesList = (RecyclerView) findViewById(R.id.veggies_listview);
        cheeseList = (RecyclerView) findViewById(R.id.cheese_listview);
        protenisList = (RecyclerView) findViewById(R.id.protein_listview);

        pricesList = (ArrayList<ItemPrices>) getIntent().getSerializableExtra(
                "item_prices");

        Collections.sort(pricesList, ItemPrices.priceSort);

        if (pricesList.size() == 4) {
            items = new String[]{"S", "M", "L", "XL"};
        } else if (pricesList.size() == 2) {
            if (pricesList.get(0).getItemTypeId() == 3) {
                items = new String[]{"L", "XL"};
            }else {
                items = new String[]{"S", "L"};
            }
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);

        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedtext = spinner.getSelectedItem().toString();

                if (selectedtext.equals("S")) {
                    size.setText("S");
                } else if (selectedtext.equals("M")) {
                    size.setText("M");
                } else if (selectedtext.equals("L")) {
                    size.setText("L");
                } else if (selectedtext.equals("XL")) {
                    size.setText("XL");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        new GetCategoryItems().execute(com.cs.oregano.Constants.ADDITIONALS_URL+getIntent().getStringExtra("additional_id"));;

        mfullPizza.setImageResource(R.drawable.pizza_size);

        mfullPizza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mfullPizza.setImageResource(R.drawable.pizza_size);

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public class GetCategoryItems extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(CustomizePizza.this);
            dialog = ProgressDialog.show(CustomizePizza.this, "",
                    "Please wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            modifiersList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(CustomizePizza.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(CustomizePizza.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONArray ja = new JSONArray(result);
                            Outer:
                            for (int i = 0; i < ja.length(); i++) {
                                JSONObject jo = ja.getJSONObject(i);
                                Modifiers mod = new Modifiers();
                                ArrayList<Additionals> additionalsList = new ArrayList<>();
                                JSONArray key = jo.getJSONArray("Key");

                                for(int j=0; j < key.length(); j++){


                                    JSONObject jo1 = key.getJSONObject(j);
                                    if(j == 0 ){
                                        String modifierName = jo1.getString("ModifierName");
                                        String modifierName_Ar = jo1.getString("ModifierName_Ar");
                                        String modifierId = jo1.getString("ModifierId");
//                                        if(modifierId.equalsIgnoreCase("1")){
//                                            continue Outer;
//                                        }
                                        mod.setModifierName(modifierName);
                                        mod.setModifierNameAr(modifierName_Ar);
                                        mod.setModifierId(modifierId);
                                    }else{

//                                        JSONObject issueObj = jo.getJSONObject("Value");
                                        Iterator iterator = jo1.keys();
                                        while(iterator.hasNext()){
                                            Additionals adis = new Additionals();
                                            String additonalId = (String)iterator.next();
                                            ArrayList<AdditionalPrices> additionalPrices = new ArrayList<>();
                                            JSONArray ja1 = jo1.getJSONArray(additonalId);

                                            for(int k = 0; k<ja1.length(); k++){
                                                JSONArray additionals = ja1.getJSONArray(k);
                                                if(k==0){
                                                    JSONObject additionalDetails = additionals.getJSONObject(0);
                                                    String additionalName = additionalDetails.getString("AdditionalName");
                                                    String additionalName_Ar = additionalDetails.getString("AdditionalName_Ar");
                                                    String images = additionalDetails.getString("Images");
                                                    String additionalId = additionalDetails.getString("AdditionalId");
                                                    adis.setAdditionalName(additionalName);
                                                    adis.setAdditionalNameAr(additionalName_Ar);
                                                    adis.setAdditionalsId(additionalId);
                                                    adis.setImages(images);
                                                    adis.setModifierId(mod.getModifierId());
                                                }else {
                                                    for(int l = 0; l<additionals.length();l++){
                                                        AdditionalPrices ap = new AdditionalPrices();
                                                        JSONObject jo2 = additionals.getJSONObject(l);
                                                        ap.setItemType(jo2.getString("Type"));
                                                        ap.setPrice(jo2.getString("Price"));
                                                        additionalPrices.add(ap);
                                                    }
                                                    adis.setAdditionalPriceList(additionalPrices);
                                                }



                                            }
                                            additionalsList.add(adis);
                                        }
                                    }


                                }

                                mod.setChildItems(additionalsList);
                                if(mod.getModifierId().equals("1") && categoryId.equalsIgnoreCase("8")){
                                    Log.i("TAG","curst " + additionalsList.size());

//                                    LinearLayoutManager llm = new LinearLayoutManager(CustomizePizza.this);
//                                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                                    crustview.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
                                    curstlist = new CurstRecyclerAdapter(CustomizePizza.this, additionalsList, language);
                                    crustview.setAdapter(curstlist);
                                }

                                if(mod.getModifierId().equals("5") && categoryId.equalsIgnoreCase("8")){
                                    Log.i("TAG","curst " + additionalsList.size());

//                                    LinearLayoutManager llm = new LinearLayoutManager(CustomizePizza.this);
//                                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                                    veggiesList.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
                                    veggieslist = new VeggiesRecyclerAdapter(CustomizePizza.this, additionalsList, language);
                                    veggiesList.setAdapter(veggieslist);
                                }

                                if(mod.getModifierId().equals("3") && categoryId.equalsIgnoreCase("8")){
                                    Log.i("TAG","curst " + additionalsList.size());

//                                    LinearLayoutManager llm = new LinearLayoutManager(CustomizePizza.this);
//                                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                                    cheeseList.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
                                    cheeselist = new CheeseRecyclerAdapter(CustomizePizza.this, additionalsList, language);
                                    cheeseList.setAdapter(cheeselist);
                                }

                                if(mod.getModifierId().equals("4") && categoryId.equalsIgnoreCase("8")){
                                    Log.i("TAG","curst " + additionalsList.size());

//                                    LinearLayoutManager llm = new LinearLayoutManager(CustomizePizza.this);
//                                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                                    protenisList.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
                                    proteinslist = new ProteinsRecyclerAdapter(CustomizePizza.this, additionalsList, language);
                                    protenisList.setAdapter(proteinslist);
                                }

                                if(mod.getModifierId().equals("1") && categoryId.equalsIgnoreCase("4")){
                                    crustList.add(mod);
                                }else if(mod.getModifierId().equals("1")){
                                    crustList.add(mod);
                                }
                                else if(mod.getModifierId().equals("9") && categoryId.equalsIgnoreCase("6")){
                                    crustList.add(mod);
                                }
                                else {
                                    modifiersList.add(mod);
                                }


//                                JSONObject key = jo.getJSONObject("Key");

//                                String categoryId = key.getString("RamadanCategoryId");
//                                String itemId = key.getString("ItemId");
//                                String itemName = key.getString("ItemName");
//                                String description = key.getString("Description");
//                                String itemName_Ar = key.getString("ItemName_Ar");
//                                String description_Ar = key.getString("Description_Ar");
//                                String additionalsId = key.getString("AdditionalsId");
//                                String modifierId = key.getString("ModifierId");
//                                String images = key.getString("Images");

//                                    String itemTypeId = jo.getString("ItemTypeId");
//                                    String price = jo.getString("Price");

//                                JSONObject issueObj = jo.getJSONObject("Value");
//                                Iterator iterator = issueObj.keys();
//                                while(iterator.hasNext()){
//                                    ItemPrices ip = new ItemPrices();
//                                    String itemTypeId = (String)iterator.next();
//
//                                    if(!itemTypeId.contains("$")) {
//                                        String price = issueObj.getString(itemTypeId);
//                                        ip.setItemTypeId(Float.parseFloat(itemTypeId));
//                                        ip.setPrice(price);
//                                        priceList.add(ip);
//                                        Log.i("TAG", ""+itemTypeId+ "  "+price);
//                                    }
//                                }


//                                items.setCategoryId(categoryId);
//                                items.setItemId(itemId);
//                                items.setItemName(itemName);
//                                items.setItemDesc(description);
//                                items.setItemName_ar(itemName_Ar);
//                                items.setItemDesc_ar(description_Ar);
//                                items.setAdditionalsId(additionalsId);
//                                items.setModifierId(modifierId);
//                                items.setImages(images);
//                                items.setItemPrices(priceList);
//
//                                Log.i("TAG", "true"+priceList.size());


//                                itemsList.add(items);


                            }







                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(CustomizePizza.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }
}
