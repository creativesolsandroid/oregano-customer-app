package com.cs.oregano.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.oregano.Constants;
import com.cs.oregano.JSONParser;
import com.cs.oregano.R;
import com.cs.oregano.adapters.OffersAdapter;
import com.cs.oregano.model.ItemPrices;
import com.cs.oregano.model.Items;
import com.cs.oregano.model.Offers;
import com.cs.oregano.widgets.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * Created by CS on 08-08-2016.
 */
public class OffersActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView title;
    ListView offerslistView;
    private ArrayList<Offers> offersList = new ArrayList<>();
    ArrayList<Items> itemsList = new ArrayList<>();
    OffersAdapter mAdapter;
    SharedPreferences languagePrefs;
    String language;
    String offerId;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        setContentView(R.layout.offers_layout);

        offerId = getIntent().getExtras().getString("offerId");
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(mSidemenuTitles[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        title = (TextView) findViewById(R.id.header_title);
        offerslistView = (ListView) findViewById(R.id.offers_listView);
//        emptyView = (TextView) findViewById(R.id.empty_view);

        if(language.equalsIgnoreCase("En")){
        if(offerId.equals("1")){
            title.setText("Individual Offers");
        }else if(offerId.equals("2")){
            title.setText("Group offers");
        }else if(offerId.equals("3")){
            title.setText("Special Offers");
        }
        }else if(language.equalsIgnoreCase("Ar")){
            if(offerId.equals("1")){
                title.setText("عروض فردية");
            }else if(offerId.equals("2")){
                title.setText("عروض للمجموعات");
            }else if(offerId.equals("3")){
                title.setText("عروض خاصة");
            }
        }

        mAdapter = new OffersAdapter(OffersActivity.this, offersList, language);
        offerslistView.setAdapter(mAdapter);

        offerslistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(offersList.get(position).getItemAvailble()){
                    ArrayList<ItemPrices> priceList = new ArrayList<>();

                    Intent intent = new Intent(OffersActivity.this, AdditionalsActivity.class);
                    intent.putExtra("item_name", itemsList.get(position).getItemName());
                    intent.putExtra("item_desc", itemsList.get(position).getItemDesc());
                    intent.putExtra("item_nameAr", itemsList.get(position).getItemName_ar());
                    intent.putExtra("item_descAr", itemsList.get(position).getItemDesc_ar());
                    intent.putExtra("item_id",itemsList.get(position).getItemId());
                    intent.putExtra("category_id",itemsList.get(position).getCategoryId());
                    intent.putExtra("additional_id", itemsList.get(position).getAdditionalsId());
                    intent.putExtra("item_prices", itemsList.get(position).getItemPrices());
                    intent.putExtra("item_image", itemsList.get(position).getImages());
                    intent.putExtra("delivery", itemsList.get(position).getIsDelivery());
                    startActivity(intent);
                }
            }
        });
        new GetOfferDetails().execute(Constants.GET_OFFERS_URL+offerId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public class GetOfferDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        ProgressDialog dialog;
        String response;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OffersActivity.this);
            dialog = ProgressDialog.show(OffersActivity.this, "",
                    "Loading offers...");
            offersList.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            offersList.clear();
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(OffersActivity.this, "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                }else{
                    if(result.equals("")){
                        Toast.makeText(OffersActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i<ja.length(); i++) {

                                    Offers offer = new Offers();
                                    JSONObject jo1 = ja.getJSONObject(i);


                                    String title = jo1.getString("OfferTitle");
                                    String titleAr = jo1.getString("OfferTitle_Ar");
                                    String desc = jo1.getString("OfferDesc");
                                    String descAr = jo1.getString("OfferDesc_Ar");
                                    String image = jo1.getString("OfferImage");

                                    try {
                                        JSONObject itemObj = jo1.getJSONObject("ItemDetails");
                                        JSONObject key = itemObj.getJSONObject("Key");
                                        Items items = new Items();

                                        String categoryId = key.getString("CategoryId");
                                        String itemId = key.getString("ItemId");
                                        String itemName = key.getString("ItemName");
                                        String description = key.getString("Description");
                                        String itemName_Ar = key.getString("ItemName_Ar");
                                        String description_Ar = key.getString("Description_Ar");
                                        String additionalsId = key.getString("AdditionalsId");
                                        String modifierId = key.getString("ModifierId");
                                        String images = key.getString("Images");
                                        String isDelivery = key.getString("IsDelivery");

//                                    String itemTypeId = jo.getString("ItemTypeId");
//                                    String price = jo.getString("Price");
                                        ArrayList<ItemPrices> priceList = new ArrayList<>();
                                        JSONObject issueObj = itemObj.getJSONObject("Value");
                                        Iterator iterator = issueObj.keys();
                                        while(iterator.hasNext()){
                                            ItemPrices ip = new ItemPrices();
                                            String itemTypeId = (String)iterator.next();

                                            if(!itemTypeId.contains("$")) {
                                                String price = issueObj.getString(itemTypeId);
                                                ip.setItemTypeId(Integer.parseInt(itemTypeId));
                                                ip.setPrice(price);
                                                priceList.add(ip);
                                            }
                                        }

                                        Collections.sort(priceList, ItemPrices.priceSort);

                                        items.setCategoryId(categoryId);
                                        items.setItemId(itemId);
                                        items.setItemName(itemName);
                                        items.setItemDesc(description);
                                        items.setItemName_ar(itemName_Ar);
                                        items.setItemDesc_ar(description_Ar);
                                        items.setAdditionalsId(additionalsId);
                                        items.setModifierId(modifierId);
                                        items.setImages(images);
                                        items.setItemPrices(priceList);
                                        items.setIsDelivery(isDelivery);

                                        itemsList.add(items);
                                        offer.setItemAvailble(true);
                                        offer.setItemsList(itemsList);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        offer.setItemAvailble(false);
                                    }

                                    offer.setOfferTitle(title);
                                    offer.setOfferTitleAr(titleAr);
                                    offer.setOfferDesc(desc);
                                    offer.setOfferDescAr(descAr);
                                    offer.setOfferImage(image);

                                    offersList.add(offer);

                                }
                            }catch (JSONException je){
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderHistoryActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
////                                if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("Oregano");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("No orders in your history")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
////                                }else if(language.equalsIgnoreCase("Ar")){
////                                    // set title
////                                    alertDialogBuilder.setTitle("د. كيف");
////
////                                    // set dialog message
////                                    alertDialogBuilder
////                                            .setMessage("البريد الالكتروني أو كلمة المرور غير صحيح")
////                                            .setCancelable(false)
////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                public void onClick(DialogInterface dialog, int id) {
////                                                    dialog.dismiss();
////                                                }
////                                            });
////                                }
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(OffersActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            mAdapter.notifyDataSetChanged();
            super.onPostExecute(result);

        }

    }
}
