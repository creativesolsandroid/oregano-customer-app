package com.cs.oregano;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cs.oregano.model.Order;
import com.cs.oregano.model.PreparationTime;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CS on 18-08-2017.
 */

public class PreparationTimeDBhelper extends SQLiteOpenHelper {

    //The Android's default system path of your application database.
    private static String DB_PATH = "/data/data/com.cs.oregano/databases/";

    private static String DB_NAME = "oregano.db";

    private SQLiteDatabase myDataBase;
    SharedPreferences versionPrefs;
    SharedPreferences.Editor versionPrefEditor;
    private final Context myContext;

    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     * @param context
     */
    public PreparationTimeDBhelper(Context context) {

        super(context, DB_NAME, null, 7);
        this.myContext = context;

        versionPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        versionPrefEditor  = versionPrefs.edit();
    }

    /**
     * Creates a empty database on the system and rewrites it with your own database.
     * */
    public void createDataBase() throws IOException{

        boolean dbExist = checkDataBase();

        if(dbExist){
            //do nothing - database already exist
        }else{

            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            this.getReadableDatabase();
            this.close();

            try {

                copyDataBase();

            } catch (IOException e) {

                throw new Error("Error copying database");

            }
        }

    }

    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase(){

        SQLiteDatabase checkDB = null;

        try{
            if (android.os.Build.VERSION.SDK_INT >= 28) {
                PreparationTimeDBhelper helper = new PreparationTimeDBhelper(myContext);
                SQLiteDatabase database = helper.getReadableDatabase();
                String filePath = database.getPath();
                database.close();
                checkDB = SQLiteDatabase.openDatabase(filePath, null, SQLiteDatabase.OPEN_READWRITE| SQLiteDatabase.NO_LOCALIZED_COLLATORS);

                if(versionPrefs.getInt("version",0) != 7) {
                    versionPrefEditor.putInt("version", 7);
                    versionPrefEditor.commit();
                    return false;
                }
            }
            else {
                String myPath = DB_PATH + DB_NAME;
                checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
            }

        }catch(SQLiteException e){

            //database does't exist yet.

        }


        if(checkDB != null){
            if(checkDB.getVersion() != 7){
                checkDB.close();
                return false;
            }else{
                checkDB.close();
                return true;
            }
        }
        return false;
    }

    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     * */
    private void copyDataBase() throws IOException{

        //Open your local db as the input stream
        InputStream myInput = myContext.getAssets().open(DB_NAME);

        // Path to the just created empty db
        String outFileName = "";
        if (android.os.Build.VERSION.SDK_INT >= 28) {
            PreparationTimeDBhelper helper = new PreparationTimeDBhelper(myContext);
            SQLiteDatabase database = helper.getReadableDatabase();
            outFileName = database.getPath();
            database.close();
//            checkDB = SQLiteDatabase.openDatabase(filePath, null, SQLiteDatabase.OPEN_READONLY);
        }
        else{
            outFileName = DB_PATH + DB_NAME;
        }

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    public void openDataBase() throws SQLException {

        //Open the database
        if (android.os.Build.VERSION.SDK_INT >= 28) {
            PreparationTimeDBhelper helper = new PreparationTimeDBhelper(myContext);
            SQLiteDatabase database = helper.getReadableDatabase();
            String filePath = database.getPath();
            database.close();
            myDataBase = SQLiteDatabase.openDatabase(filePath, null, SQLiteDatabase.OPEN_READWRITE| SQLiteDatabase.NO_LOCALIZED_COLLATORS);
            Log.d("TAG", "openDataBase: "+myDataBase);
        }
        else{
            String myPath = DB_PATH + DB_NAME;
            myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
        }

    }

    @Override
    public synchronized void close() {

        if(myDataBase != null)
            myDataBase.close();

        super.close();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DB_NAME);
//        db.execSQL("DROP TABLE IF EXISTS " + PHONE_TABLE);
    }

    // Add your public helper methods to access and get content from the database.
    // You could return cursors by doing "return myDataBase.query(....)" so it'd be easy
    // to you to create adapters for your views.


    public ArrayList<PreparationTime> getPrepTime(String catId){
        ArrayList<PreparationTime> timings = new ArrayList<>();
        String selectQuery = "SELECT * FROM  PreparationTimeTbl where categoryId = "+catId;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                PreparationTime sc = new PreparationTime();
                sc.setCatId(cursor.getString(0));
                sc.setCatName(cursor.getString(1));
                sc.setQty1(cursor.getString(2));
                sc.setQty2(cursor.getString(3));
                sc.setQty3(cursor.getString(4));
                sc.setQty4(cursor.getString(5));
                sc.setQty5(cursor.getString(6));
                sc.setQty5Plus(cursor.getString(7));
                timings.add(sc);
            } while (cursor.moveToNext());
        }

        return timings;
    }

    public void insertOrder(HashMap<String, String> queryValues) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("mainCategoryId", queryValues.get("mainCategoryId"));
        values.put("subCategoryId", queryValues.get("subCategoryId"));
        values.put("itemId", queryValues.get("itemId"));
        values.put("itemName", queryValues.get("itemName"));
        values.put("itemImage", queryValues.get("itemImage"));
        values.put("description", queryValues.get("description"));
        values.put("itemTypeId", queryValues.get("itemTypeId"));
        values.put("itemPrice", queryValues.get("itemPrice"));
        values.put("itemPriceAd", queryValues.get("itemPriceAd"));
        values.put("additionalId", queryValues.get("additionalId"));
        values.put("additionalName", queryValues.get("additionalName"));
        values.put("additionalPrice", queryValues.get("additionalPrice"));
        values.put("qty", queryValues.get("qty"));
        values.put("totalAmount", queryValues.get("totalAmount"));
        values.put("size", queryValues.get("size"));
        values.put("comment", queryValues.get("comment"));
        values.put("itemNameAr", queryValues.get("itemNameAr"));
        values.put("descriptionAr", queryValues.get("descriptionAr"));
        values.put("additionalNameAr", queryValues.get("additionalNameAr"));
        values.put("nonDelivery",queryValues.get("nonDelivery"));

        database.insert("OrderTable", null, values);
        database.close();
    }

    public void updateOrder(String qty, String price, String orderId){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("qty", qty);
        values.put("price", price);
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "UPDATE OrderTable SET qty ="+qty+ ",totalAmount ="+ price +" where orderId ="+orderId;
        database.execSQL(updateQuery);
    }


    public ArrayList<Order> getOrderInfo(){
        ArrayList<Order> orderList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Order sc = new Order();
                sc.setOrderId(cursor.getString(0));
                sc.setMainCategoryId(cursor.getString(1));
                sc.setSubCategoryId(cursor.getString(2));
                sc.setItemId(cursor.getString(3));
                sc.setItemName(cursor.getString(4));
                sc.setItemImage(cursor.getString(5));
                sc.setDescription(cursor.getString(6));
                sc.setItemTypeId(cursor.getString(7));
                sc.setItemPrice(cursor.getString(8));
                sc.setItemPriceAd(cursor.getString(9));
                sc.setAdditionalId(cursor.getString(10));
                sc.setAdditionalName(cursor.getString(11));
                sc.setAdditionalPrice(cursor.getString(12));
                sc.setQty(cursor.getString(13));
                sc.setTotalAmount(cursor.getString(14));
                sc.setSize(cursor.getString(15));
                sc.setComment(cursor.getString(16));
                sc.setItemNameAr(cursor.getString(17));
                sc.setDescriptionAr(cursor.getString(18));
                sc.setAdditionalNameAr(cursor.getString(19));
                sc.setNonDelivery(cursor.getString(20));
                orderList.add(sc);
            } while (cursor.moveToNext());
        }
        return orderList;
    }

    public ArrayList<Order> getItemOrderInfo(String itemId){
        ArrayList<Order> orderList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  OrderTable where itemId = "+itemId;
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Order sc = new Order();
                sc.setOrderId(cursor.getString(0));
                sc.setMainCategoryId(cursor.getString(1));
                sc.setSubCategoryId(cursor.getString(2));
                sc.setItemId(cursor.getString(3));
                sc.setItemName(cursor.getString(4));
                sc.setItemImage(cursor.getString(5));
                sc.setDescription(cursor.getString(6));
                sc.setItemTypeId(cursor.getString(7));
                sc.setItemPrice(cursor.getString(8));
                sc.setItemPriceAd(cursor.getString(9));
                sc.setAdditionalId(cursor.getString(10));
                sc.setAdditionalName(cursor.getString(11));
                sc.setAdditionalPrice(cursor.getString(12));
                sc.setQty(cursor.getString(13));
                sc.setTotalAmount(cursor.getString(14));
                sc.setSize(cursor.getString(15));
                sc.setComment(cursor.getString(16));
                sc.setItemNameAr(cursor.getString(17));
                sc.setDescriptionAr(cursor.getString(18));
                sc.setAdditionalNameAr(cursor.getString(19));
                sc.setNonDelivery(cursor.getString(20));
                orderList.add(sc);
            } while (cursor.moveToNext());
        }
        return orderList;
    }

    public ArrayList<Order> getNonDeliveryItems() {
        ArrayList<Order> orderList = new ArrayList<>();
        String selectQuery = "SELECT * FROM  OrderTable where nonDelivery=?;";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, new String[] {"false"});
        if (cursor.moveToFirst()) {
            do {
                Order sc = new Order();
                sc.setOrderId(cursor.getString(0));
                sc.setMainCategoryId(cursor.getString(1));
                sc.setSubCategoryId(cursor.getString(2));
                sc.setItemId(cursor.getString(3));
                sc.setItemName(cursor.getString(4));
                sc.setItemImage(cursor.getString(5));
                sc.setDescription(cursor.getString(6));
                sc.setItemTypeId(cursor.getString(7));
                sc.setItemPrice(cursor.getString(8));
                sc.setItemPriceAd(cursor.getString(9));
                sc.setAdditionalId(cursor.getString(10));
                sc.setAdditionalName(cursor.getString(11));
                sc.setAdditionalPrice(cursor.getString(12));
                sc.setQty(cursor.getString(13));
                sc.setTotalAmount(cursor.getString(14));
                sc.setSize(cursor.getString(15));
                sc.setComment(cursor.getString(16));
                sc.setItemNameAr(cursor.getString(17));
                sc.setDescriptionAr(cursor.getString(18));
                sc.setAdditionalNameAr(cursor.getString(19));
                sc.setNonDelivery(cursor.getString(20));
                orderList.add(sc);
            } while (cursor.moveToNext());
        }
        return orderList;
    }

    public int getItemOrderCount(String itemId){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(qty) FROM  OrderTable where itemId="+itemId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        return cnt;
    }

    public int getCategoryOrderCount(String categoryID){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(qty) FROM  OrderTable where mainCategoryId="+categoryID;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        return cnt;
    }

    public float getItemOrderPrice(String itemId){
        float cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(price) FROM  OrderTable where orderId="+itemId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getFloat(0);
        }
        return cnt;
    }

    public int getSubcatOrderCount(String subCategoryId){
        int cnt = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        String deleteQuery = "SELECT SUM(qty) FROM  OrderTable where subCategoryId="+subCategoryId;
        Cursor c=database.rawQuery(deleteQuery, null);
        if(c.moveToFirst())
        {
            cnt  = c.getInt(0);
        }
        return cnt;
    }

    public int getTotalOrderQty(){
        int qty = 0;
        String selectQuery = "SELECT itemId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        try {
            Cursor cur = database.rawQuery("SELECT SUM(qty) FROM OrderTable", null);

            if (cur.moveToFirst()) {
                qty = cur.getInt(0);
            }
        }catch (SQLiteException sqle){
            sqle.printStackTrace();
        }
        return qty;
    }

    public float getTotalOrderPrice(){
        float qty = 0;
        String selectQuery = "SELECT itemId FROM  OrderTable";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cur = database.rawQuery("SELECT SUM(totalAmount) FROM OrderTable", null);
        if(cur.moveToFirst())
        {
            qty  = cur.getFloat(0);
        }

        return qty;
    }


    public void deleteItemFromOrder(String orderId){
        SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable where orderId = "+orderId;
        database.execSQL(updateQuery);

    }

    public void deleteItemFromOrderID(String orderId, String size) {
        SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable where itemId = " + orderId + " AND itemTypeId =" + size;
        database.execSQL(updateQuery);

    }

    public void deleteOrderTable(){
        SQLiteDatabase database = this.getWritableDatabase();

//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "DELETE FROM OrderTable";
        database.execSQL(updateQuery);
    }

    public void updateComment(String orderId, String comment){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
//		return database.update("news_count", values, "newsId" + " = ?", new String[] { queryValues.get("newsId") });
        String updateQuery = "UPDATE OrderTable SET comment = '"+comment+"' where orderId ="+orderId;
        database.execSQL(updateQuery);
    }
}
