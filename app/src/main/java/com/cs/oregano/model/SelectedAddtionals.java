package com.cs.oregano.model;

import java.util.ArrayList;

/**
 * Created by SKT on 16-02-2016.
 */
public class SelectedAddtionals {
    String additionalId, modifierId, additionalName, additionalNameAr, price;
    ArrayList<AdditionalPrices> additionalPriceList;

    public ArrayList<AdditionalPrices> getAdditionalPriceList() {
        return additionalPriceList;
    }

    public void setAdditionalPriceList(ArrayList<AdditionalPrices> additionalPriceList) {
        this.additionalPriceList = additionalPriceList;
    }
    public String getAdditionalId() {
        return additionalId;
    }

    public void setAdditionalId(String additionalId) {
        this.additionalId = additionalId;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAdditionalName() {
        return additionalName;
    }

    public void setAdditionalName(String additionalName) {
        this.additionalName = additionalName;
    }

    public String getAdditionalNameAr() {
        return additionalNameAr;
    }

    public void setAdditionalNameAr(String additionalNameAr) {
        this.additionalNameAr = additionalNameAr;
    }
}
