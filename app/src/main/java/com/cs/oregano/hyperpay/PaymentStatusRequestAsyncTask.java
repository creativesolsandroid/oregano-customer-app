package com.cs.oregano.hyperpay;

import android.os.AsyncTask;
import android.util.Log;

import com.cs.oregano.Rest.APIInterface;
import com.cs.oregano.Rest.ApiClient;
import com.cs.oregano.model.MyCards;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Represents an async task to request a payment status from the server.
 */
public class PaymentStatusRequestAsyncTask extends AsyncTask<String, Void, MyCards> {

    private PaymentStatusRequestListener listener;
    private MyCards cardsResponse = null;

    public PaymentStatusRequestAsyncTask(PaymentStatusRequestListener listener) {
        this.listener = listener;
    }

    @Override
    protected MyCards doInBackground(String... params) {
//        if (params.length != 1) {
//            return null;
//        }

        // prepare input json
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("resourcePath", params[0]);
            parentObj.put("userId", params[1]);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final APIInterface apiService =
                ApiClient.getClient().create(APIInterface.class);

        Call<MyCards> call = apiService.getPaymentStatus(
                RequestBody.create(MediaType.parse("application/json"), parentObj.toString()));
        call.enqueue(new Callback<MyCards>() {
            @Override
            public void onResponse(Call<MyCards> call, Response<MyCards> response) {
                Log.d("TAG", "onResponse: " + response);
                if (response.isSuccessful()) {
                    MyCards checkOutIdResponse = response.body();
                    try {
                        if (checkOutIdResponse.getStatus()) {
                            cardsResponse = checkOutIdResponse;

                            if (listener != null) {
                                listener.onPaymentStatusReceived(cardsResponse);
                                Log.i("TAG", "onResponse: " + cardsResponse);

                            }
                        } else {
                            // status false case
                            if (listener != null) {
                                listener.onErrorOccurred();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (listener != null) {
                            listener.onErrorOccurred();
                        }
                    }
                } else {
                    if (listener != null) {
                        listener.onErrorOccurred();
                    }
                }
            }

            @Override
            public void onFailure(Call<MyCards> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.toString());
                if (listener != null) {
                    listener.onErrorOccurred();
                }
            }
        });

        return null;
    }

    @Override
    protected void onPostExecute(MyCards paymentStatus) {

    }
}
