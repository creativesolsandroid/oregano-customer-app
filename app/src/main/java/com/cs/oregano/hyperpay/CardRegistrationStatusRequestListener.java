package com.cs.oregano.hyperpay;


import com.cs.oregano.model.CardRegistration;

public interface CardRegistrationStatusRequestListener {

    void onErrorOccurred();
    void onCardRegistrationStatusReceived(CardRegistration paymentStatus);
}
