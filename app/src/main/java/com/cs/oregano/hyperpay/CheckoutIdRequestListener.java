package com.cs.oregano.hyperpay;


public interface CheckoutIdRequestListener {

    void onCheckoutIdReceived(String checkoutId);
}
