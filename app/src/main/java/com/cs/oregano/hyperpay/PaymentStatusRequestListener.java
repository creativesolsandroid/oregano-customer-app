package com.cs.oregano.hyperpay;


import com.cs.oregano.model.MyCards;

public interface PaymentStatusRequestListener {

    void onErrorOccurred();
    void onPaymentStatusReceived(MyCards paymentStatus);
}
