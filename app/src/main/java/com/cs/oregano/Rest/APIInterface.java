package com.cs.oregano.Rest;

import com.cs.oregano.model.CardRegistration;
import com.cs.oregano.model.CheckOutId;
import com.cs.oregano.model.DeleteCardList;
import com.cs.oregano.model.MyCards;
import com.cs.oregano.model.SaveCardToDB;
import com.cs.oregano.model.SavedCards;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface APIInterface {

//    Hyperpay
    @POST("HyperpayAPI/CheckoutRequestFromSDK")
    Call<CheckOutId> generateCheckOutId(@Body RequestBody body);

    @POST("HyperpayAPI/paymentStatus")
    Call<MyCards> getPaymentStatus(@Body RequestBody body);

    @POST("HyperpayAPI/AddCardStatus")
    Call<CardRegistration> addCardStatus(@Body RequestBody body);

    @POST("HyperpayAPI/InsertCreditCardDetails")
    Call<SaveCardToDB> insertCreditCard(@Body RequestBody body);

    @POST("HyperpayAPI/GetCreditCardDetails")
    Call<SavedCards> GetAllSavedCards(@Body RequestBody body);

    @POST("HyperpayAPI/deleteCard")
    Call<DeleteCardList> deleteCard(@Body RequestBody body);

 }
