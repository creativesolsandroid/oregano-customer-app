package com.cs.oregano.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.oregano.Constants;
import com.cs.oregano.R;
import com.cs.oregano.model.Additionals;

import java.util.ArrayList;

public class CurstRecyclerAdapter extends RecyclerView.Adapter<CurstRecyclerAdapter.ViewHolder> {

    ArrayList<Additionals> orderList = new ArrayList<>();

    private LayoutInflater mInflater;
    String language;
    Activity parentActivity;
    public Context context;


    public CurstRecyclerAdapter(Context context, ArrayList<Additionals> orderList, String language) {
        this.mInflater = LayoutInflater.from(context);
        this.orderList = orderList;
        this.parentActivity = parentActivity;
        this.language = language;
        this.context = context;
        Log.i("TAG","curst size "+this.orderList.size());
    }

    @Override
    public CurstRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i("TAG","curst size1 "+orderList.size());
        View view = null;
//        if (language.equalsIgnoreCase("En")) {
            view = mInflater.inflate(R.layout.curst_list, parent, false);
//        } else if (language.equalsIgnoreCase("Ar")) {
//            view = mInflater.inflate(R.layout.curst_list, parent, false);
//        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CurstRecyclerAdapter.ViewHolder holder, int position) {
        Log.i("TAG","curst size2 "+orderList.size());

        holder.title.setText(orderList.get(position).getAdditionalName());
        Glide.with(context).load(Constants.IMAGE_URL +orderList.get(position).getImages()).placeholder(R.drawable.app_logo).into(holder.curstImage);

        holder.itemImage.setVisibility(View.GONE);

            holder.curstImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                        holder.itemImage.setVisibility(View.VISIBLE);

                    if (holder.itemImage.performClick()){

                        holder.itemImage.setVisibility(View.GONE);

                    }

                }
            });

        Log.i("TAG","curst size "+orderList.size());
        Log.i("TAG","name "+orderList.get(position).getAdditionalName());
        Log.i("TAG","img "+orderList.get(position).getImages());

    }

    @Override
    public int getItemCount() {
        Log.i("TAG","curst size3 "+orderList.size());
        return orderList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView itemImage;
        ImageView curstImage;

        public ViewHolder(View itemView) {
            super(itemView);
            Log.i("TAG","curst size4 "+orderList.size());

            title = itemView.findViewById(R.id.curst_name);
            itemImage = itemView.findViewById(R.id.curstcheck);
            curstImage = itemView.findViewById(R.id.curst_img_layout);


        }
    }

}
